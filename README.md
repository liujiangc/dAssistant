# dAssistant

#### 介绍

功能1：全局捕捉APP发生的致命bug；
功能2：发生异常后重启应用，并发送异常信息到自定义邮箱/自定义处理方法；
功能3：发送文本邮件，附件邮件以及Html邮件。

#### 效果图
![异常发送到邮箱的效果图](https://images.gitee.com/uploads/images/2019/0708/150642_7fb134f8_5114946.png "36Z]QUBBVIA_KRC$]D@M{)8.png")

#### 安装教程

1. 在项目的build.gradle中添加 

    allprojects {
       repositories {     
            ..  
            maven { url 'https://www.jitpack.io' }
       }
    }

2. 在APP的build.gradle中添加 

    dependencies {
         ..
         implementation 'com.gitee.liujiangc:dAssistant:0.1.8'
    } 


#### 使用说明

1. 在MyApplication中完成初始化，在程序发生致命错误的时候将重启APP,并将异常信息发送邮件指定邮箱

public class MyApplication extends Application {

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        /** 初始化（如果只需要发送邮件的功能不需要初始化）
        *  init参数1：邮箱配置信息
        *  init参数2：错误信息回调
        *  init可传单个参数也可传null
        **/
        DeathAssistant.getInstance(getInstance(), true).init(getEmailBean(),null); 
    }

     /**
     * 邮箱配置信息
     * @return
     */
    public EmailBean getEmailBean() {
        EmailBean eb = new EmailBean();
        eb.setHost("smtp.qq.com");//邮箱服务器地址 这个是QQ邮箱的
        eb.setPort("587");//邮箱服务器端口
        eb.setApp_name(this.getString(R.string.app_name));//你的app名称
        eb.setUser_name("test_user_id");//如果APP有登录模块，可以把用户的id回传
        eb.setToEmail("填写你要接收邮件的地址@qq.com");//接收邮件的地址
        eb.setFormEmail("填写你要发送邮件的地址@qq.com");//发送邮件的地址
        eb.setPassword("填写你申请获得的授权码");//登录第三方客户端邮箱的授权码
        // QQ邮箱获取授权码流程：登录网页版QQ邮箱 - 设置 - 账户 - 开启POP3/SMTP服务 - 开启IMAP/SMTP服务 - 生成授权码
        // 新申请的QQ需要使用两周以上才能申请授权码
        eb.setTitle("错误报告");//邮箱的标题
        return eb;
    }
}

2. AndroidManifest.xml 
 
     <application 
        android:name=".MyApplication" 
        .. 
      > 

3. 发送纯文本的邮件

    EmailBean eb = MyApplication.getInstance().getEmailBean();
    eb.setTitle("纯文本的邮件");
    final MailInfo mailInfo = creatMail(eb);
    mailInfo.setContent("锄禾日当午，汗滴禾下土。");
    new Thread(new Runnable() {
        @Override
        public void run() {
            MailSender sms = new MailSender();
            sms.sendTextMail(mailInfo);
        }
    }).start();

4. 发送带附件的邮件 - 需要获取读取权限

    EmailBean eb_file = MyApplication.getInstance().getEmailBean();
    eb_file.setTitle("含附件的邮件");
    final MailInfo mailInfo_file = creatMail(eb_file);
    mailInfo_file.setContent("谁知盘中餐，粒粒皆辛苦。");
    new Thread(new Runnable() {
        @Override
        public void run() {
            MailSender sms = new MailSender();
            sms.sendFileMail(mailInfo_file, new File(Environment.getExternalStorageDirectory().getPath() + "/mes.txt"));
        }
    }).start();

5. 发送Html文本的邮件

    EmailBean eb_html = MyApplication.getInstance().getEmailBean();
    eb_html.setTitle("含Html标签的邮件");
    final MailInfo mailInfo_html = creatMail(eb_html);
    mailInfo_html.setContent("<a href='http://192.168.0.1'>跳转</a>");
    new Thread(new Runnable() {
        @Override
        public void run() {
            MailSender sms = new MailSender();
            sms.sendHtmlMail(mailInfo_html);
        }
    }).start();

#### 混淆配置

    # Gson 
    -dontwarn sun.misc.**
    -keepclass sun.misc.**{*;}
    -dontwarn com.google.**
    -keepclass com.google.** {*;} 
    # 邮件类
    -dontwarn com.sun.activation.registries.**
    -keep class com.sun.activation.registries.** { *;}
    -dontwarn javax.activation.**
    -keep class javax.activation.** { *;}
    -dontwarn myjava.awt.datatransfer.**
    -keep class myjava.awt.datatransfer.**{ *;}
    -dontwarn org.apache.harmony.**
    -keep class org.apache.harmony.** { *;}
    -dontwarn com.sun.mail.**
    -keep class com.sun.mail.** { *;}
    # 本demo
    -dontwarn cn.ljcdada.deathassistant.**
    -keep class cn.ljcdada.deathassistant.** { *;}
 