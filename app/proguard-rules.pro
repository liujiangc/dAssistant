# 通用
-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-keepattributes *Annotation*,InnerClasses
-keepattributes Signature
-keepattributes SourceFile,LineNumberTable
-keepattributes EnclosingMethod
-optimizations !code/simplification/cast,!field/*,!class/merging/*

-keep class android.support.** {*;}
-keep public class * extends android.support.v4.**
-keep public class * extends android.support.v7.**
-keep public class * extends android.support.annotation.**

#自定义混淆字典 配和名字一样 成员的名字 类名 包名 各种都可以设置字典
-obfuscationdictionary dictoO0.txt
-classobfuscationdictionary dictoO0.txt
-packageobfuscationdictionary dictoO0.txt

