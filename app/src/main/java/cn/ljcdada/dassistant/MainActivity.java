package cn.ljcdada.dassistant;

import android.Manifest;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.File;
import java.util.List;

import cn.ljcdada.deathassistant.email.MailInfo;
import cn.ljcdada.deathassistant.email.MailSender;
import cn.ljcdada.deathassistant.email.EmailBean;
import pub.devrel.easypermissions.EasyPermissions;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static cn.ljcdada.deathassistant.email.SendMailUtil.creatMail;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks {


    String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        findViewById(R.id.main_btn_kill).setOnClickListener(this);
        findViewById(R.id.main_btn_text).setOnClickListener(this);
        findViewById(R.id.main_btn_file).setOnClickListener(this);
        findViewById(R.id.main_btn_html).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_btn_kill:
                /** 制造一个空指针错误模仿其他异常使程序崩溃 重启后将发送错误邮件**/
                List<String> list = null;
                list.add("");
                break;
            case R.id.main_btn_text:
                EmailBean eb = MyApplication.getInstance().getEmailBean();
                eb.setTitle("纯文本的邮件");
                final MailInfo mailInfo = creatMail(eb);
                mailInfo.setContent("锄禾日当午，汗滴禾下土。");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        MailSender sms = new MailSender();
                        sms.sendTextMail(mailInfo);
                    }
                }).start();
                break;
            case R.id.main_btn_file:
                if (EasyPermissions.hasPermissions(MainActivity.this, perms)) {
                    EmailBean eb_file = MyApplication.getInstance().getEmailBean();
                    eb_file.setTitle("含附件的邮件");
                    final MailInfo mailInfo_file = creatMail(eb_file);
                    mailInfo_file.setContent("谁知盘中餐，粒粒皆辛苦。");
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            MailSender sms = new MailSender();
                            sms.sendFileMail(mailInfo_file, new File(Environment.getExternalStorageDirectory().getPath() + "/mes.txt"));
                        }
                    }).start();
                } else {
                    EasyPermissions.requestPermissions(MainActivity.this, "需要这些权限获取文件",
                            102, perms);
                }
                break;
            case R.id.main_btn_html:
                EmailBean eb_html = MyApplication.getInstance().getEmailBean();
                eb_html.setTitle("含Html标签的邮件");
                final MailInfo mailInfo_html = creatMail(eb_html);
                mailInfo_html.setContent("<a href='http://192.168.0.1'>跳转</a>");
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        MailSender sms = new MailSender();
                        sms.sendHtmlMail(mailInfo_html);
                    }
                }).start();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        if (requestCode == 102) {

        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

}
