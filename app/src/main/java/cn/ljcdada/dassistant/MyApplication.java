package cn.ljcdada.dassistant;

import android.app.Application;

import cn.ljcdada.deathassistant.DeathAssistant;
import cn.ljcdada.deathassistant.OnKillAppListener;
import cn.ljcdada.deathassistant.email.EmailBean;


public class MyApplication extends Application {

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        /** 初始化方法（如果只需要发送邮件的功能不需要初始化）
         *  init参数1：邮箱配置信息
         *  init参数2：错误信息回调
         *  init可传单个参数也可传null
         **/
        DeathAssistant.getInstance(getInstance(), true).init(getEmailBean(),null);//捕获到异常以邮件的形式发送
    }

    /***
     * 邮箱配置信息
     * @return
     */
    public EmailBean getEmailBean() {
        EmailBean eb = new EmailBean();
        eb.setHost("smtp.qq.com");//邮箱服务器地址 这个是QQ邮箱的
        eb.setPort("587");//邮箱服务器端口
        eb.setApp_name(getString(R.string.app_name));//你的app名称
        eb.setUser_name("test_user_id");//如果APP有登录模块，可以把用户的id回传
        eb.setToEmail("请填写接收地址@qq.com");//接收邮件的地址
        eb.setFormEmail("请填写发送地址@qq.com");//发送邮件的地址
        eb.setPassword("请填写申请的授权码");//登录第三方客户端邮箱的授权码
        // QQ邮箱获取授权码流程：登录网页版QQ邮箱 - 设置 - 账户 - 开启POP3/SMTP服务 - 开启IMAP/SMTP服务 - 生成授权码
        // 新QQ需要两周以上才能申请授权码
        eb.setTitle("错误报告");//邮箱的标题
        return eb;
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

}