#需要添加的混淆规则
-dontwarn com.sun.activation.registries.**
-keep class com.sun.activation.registries.** { *;}
-dontwarn javax.activation.**
-keep class javax.activation.** { *;}
-dontwarn myjava.awt.datatransfer.**
-keep class myjava.awt.datatransfer.**{ *;}
-dontwarn org.apache.harmony.**
-keep class org.apache.harmony.** { *;}
-dontwarn com.sun.mail.**
-keep class com.sun.mail.** { *;}

-dontwarn cn.ljcdada.deathassistant.**
-keep class cn.ljcdada.deathassistant.** { *;}