package cn.ljcdada.deathassistant;

import android.content.Context;
import android.content.Intent;

import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;

import cn.ljcdada.deathassistant.email.EmailBean;
import cn.ljcdada.deathassistant.other.SP;

public class DeathAssistant implements Thread.UncaughtExceptionHandler {

    private final static String isPutEmail = "isPutEmail";
    private final static String Email_time = "Email_time";
    private final static String Email_message = "Email_message";
    private final static String Email_json = "Email_json";

    public void init(EmailBean emailBean) {
        if ((boolean) SP.getParam(mContext, isPutEmail, false)) {
            if (emailBean != null) {
                Intent it = new Intent(mContext, PutEmailService.class);
                emailBean.setMessages((String) SP.getParam(mContext, Email_message, ""));
                emailBean.setTime((String) SP.getParam(mContext, Email_time, ""));
                emailBean.setJson((String) SP.getParam(mContext, Email_json, ""));
                it.putExtra("email", emailBean);
                mContext.startService(it);
            }
            SP.clearAll(mContext);
        }
        Thread.setDefaultUncaughtExceptionHandler(myCrashHandler);
    }

    public void init(OnKillAppListener onKillAppListener) {
        if ((boolean) SP.getParam(mContext, isPutEmail, false)) {
            if (onKillAppListener != null) {
                String mes = (String) SP.getParam(mContext, Email_message, "");
                String time = (String) SP.getParam(mContext, Email_time, "");
                String json = (String) SP.getParam(mContext, Email_json, "");
                onKillAppListener.onKill(time, mes, json);
            }
            SP.clearAll(mContext);
        }
        Thread.setDefaultUncaughtExceptionHandler(myCrashHandler);
    }

    public void init(EmailBean emailBean, OnKillAppListener onKillAppListener) {
        if ((boolean) SP.getParam(mContext, isPutEmail, false)) {
            String mes = (String) SP.getParam(mContext, Email_message, "");
            String time = (String) SP.getParam(mContext, Email_time, "");
            String json = (String) SP.getParam(mContext, Email_json, "");
            if (emailBean != null) {
                Intent it = new Intent(mContext, PutEmailService.class);
                emailBean.setMessages(mes);
                emailBean.setTime(time);
                emailBean.setJson(json);
                it.putExtra("email", emailBean);
                mContext.startService(it);
            }
            if (onKillAppListener != null) {
                onKillAppListener.onKill(time, mes, json);
            }
            SP.clearAll(mContext);
        }
        Thread.setDefaultUncaughtExceptionHandler(myCrashHandler);
    }

    private static DeathAssistant myCrashHandler;

    private boolean isKillProcess = true; //发生异常是否重启应用程序

    private Context mContext;

    private DeathAssistant(Context context, boolean isKillProcess) {
        this.mContext = context;
        this.isKillProcess = isKillProcess;
    }

    public static synchronized DeathAssistant getInstance(Context context, boolean isKillProcess) {
        if (null == myCrashHandler) {
            myCrashHandler = new DeathAssistant(context, isKillProcess);
        }
        return myCrashHandler;
    }

    public static synchronized DeathAssistant getInstance(Context context) {
        if (null == myCrashHandler) {
            myCrashHandler = new DeathAssistant(context,true);
        }
        return myCrashHandler;
    }

    public void uncaughtException(Thread thread, Throwable throwable) {
        /****
         * 储存错误内容，重启后发送错误邮件
         */
        SP.setParam(mContext, isPutEmail, true);//标志位 true 代表有错误发生
        SP.setParam(mContext, Email_time, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss E").format(new Date()));//时间
        if (throwable != null) {
            SP.setParam(mContext, Email_message, throwable.getMessage() + ""); //错误内容
            SP.setParam(mContext, Email_json, new GsonBuilder().disableHtmlEscaping().serializeNulls().create().toJson(throwable) + "");//错误详情
        } else {
            SP.setParam(mContext, Email_message, "未知错误 - 502");
            SP.setParam(mContext, Email_json, "{\"Message\":\"未知错误 - 502\"}");
        }
        throwable.printStackTrace();
        if (isKillProcess) {
            mContext.startActivity(mContext.getPackageManager().getLaunchIntentForPackage(mContext.getPackageName()));// 重启应用
            android.os.Process.killProcess(android.os.Process.myPid()); //杀死当前的程序
        }
    }
}