package cn.ljcdada.deathassistant;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import cn.ljcdada.deathassistant.email.EmailBean;
import cn.ljcdada.deathassistant.email.MailInfo;
import cn.ljcdada.deathassistant.email.MailSender;
import cn.ljcdada.deathassistant.other.APKVersionCodeUtils;

import static cn.ljcdada.deathassistant.email.SendMailUtil.creatMail;

public class PutEmailService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        EmailBean mEmail = (EmailBean) intent.getExtras().getSerializable("email");
        final MailInfo mailInfo = creatMail(mEmail);
        mailInfo.setContent(addMessages(mEmail));
        new Thread(new Runnable() {
            @Override
            public void run() {
                MailSender sms = new MailSender();
                sms.sendHtmlMail(mailInfo);
            }
        }).start();
//        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }


    private String addMessages(EmailBean mEmail) {
        String errorMes = mEmail.getMessages();//错误信息
        String jsonMes = stringToJSON(mEmail.getJson());//错误详情
        String str_time = mEmail.getTime();//发生时间
        String brand = android.os.Build.BRAND;//品牌
        String model = android.os.Build.MODEL;//型号
        String systemVersion = android.os.Build.VERSION.RELEASE;//手机系统版本号
        String SDKVersion = android.os.Build.VERSION.SDK;//SDK 版本
        String versionCode = APKVersionCodeUtils.getVersionCode(this) + "";//版本号
        String versionName = APKVersionCodeUtils.getVerName(this);//版本名称
        String html =
                "<table width=\"auto\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> \n" +

                        "<tr> \n" +
                        "<td valign=\"top\" style=\"white-space: nowrap;\">产出信息：</td> \n" +
                        "<td>" + "<font face=\"verdana\" color=\"Black\">" + "From Android - " + mEmail.getApp_name() + "</font>" + "</td> \n" +
                        "</tr> \n" +

                        "<tr> \n" +
                        "<td valign=\"top\" style=\"white-space: nowrap;\">错误信息：</td> \n" +
                        "<td>" + "<font face=\"verdana\" color=\"Black\">" + errorMes + "</font>" + "</td> \n" +
                        "</tr> \n" +

                        "<tr> \n" +
                        "<td valign=\"top\" style=\"white-space: nowrap;\">发生时间：</td> \n" +
                        "<td>" + "<font face=\"verdana\" color=\"Black\">" + str_time + "</font>" + "</td> \n" +
                        "</tr> \n" +
                        (mEmail.getUser_name().length() > 0 ? (
                                "<tr> \n" +
                                        "<td valign=\"top\" style=\"white-space: nowrap;\">用户账号：</td> \n" +
                                        "<td>" + "<font face=\"verdana\" color=\"Black\">" + mEmail.getUser_name() + "</font>" + "</td> \n" +
                                        "</tr> \n"
                        ) : "") +

                        "<tr> \n" +
                        "<td valign=\"top\" style=\"white-space: nowrap;\">手机品牌：</td> \n" +
                        "<td>" + "<font face=\"verdana\" color=\"Black\">" + brand + "</font>" + "</td> \n" +
                        "</tr> \n" +

                        "<tr> \n" +
                        "<td valign=\"top\" style=\"white-space: nowrap;\">手机型号：</td> \n" +
                        "<td>" + "<font face=\"verdana\" color=\"Black\">" + model + "</font>" + "</td> \n" +
                        "</tr> \n" +

                        "<tr> \n" +
                        "<td valign=\"top\" style=\"white-space: nowrap;\">系统版本：</td> \n" +
                        "<td>" + "<font face=\"verdana\" color=\"Black\">" + systemVersion + "</font>" + "</td> \n" +
                        "</tr> \n" +

                        "<tr> \n" +
                        "<td valign=\"top\" style=\"white-space: nowrap;\">SDK版本：</td> \n" +
                        "<td>" + "<font face=\"verdana\" color=\"Black\">" + SDKVersion + "</font>" + "</td> \n" +
                        "</tr> \n" +

                        "<tr> \n" +
                        "<td valign=\"top\" style=\"white-space: nowrap;\">APP版本：</td> \n" +
                        "<td>" + "<font face=\"verdana\" color=\"Black\">" + versionCode + "</font>" + "</td> \n" +
                        "</tr> \n" +

                        "<tr> \n" +
                        "<td valign=\"top\" style=\"white-space: nowrap;\">版本名称：</td> \n" +
                        "<td>" + "<font face=\"verdana\" color=\"Black\">" + versionName + "</font>" + "</td> \n" +
                        "</tr> \n" +

                        "<tr> \n" +
                        "<td valign=\"top\" style=\"white-space: nowrap;\">错误详情：</td> \n" +
                        "<td>" +
                        "<font face=\"verdana\" color=\"green\"><pre style=\"margin:2px;\">" + jsonMes + "</pre></font>" +
                        "</td> \n" +
                        "</tr> \n" +

                        "</table> ";

        return html;
    }

    /**
     * @date 2017/8/24
     * @description 将字符串格式化成JSON的格式
     */
    public static String stringToJSON(String strJson) {
        // 计数tab的个数
        int tabNum = 0;
        StringBuffer jsonFormat = new StringBuffer();
        int length = strJson.length();

        char last = 0;
        for (int i = 0; i < length; i++) {
            char c = strJson.charAt(i);
            if (c == '{') {
                tabNum++;
                jsonFormat.append(c + "\n");
                jsonFormat.append(getSpaceOrTab(tabNum));
            } else if (c == '}') {
                tabNum--;
                jsonFormat.append("\n");
                jsonFormat.append(getSpaceOrTab(tabNum));
                jsonFormat.append(c);
            } else if (c == ',') {
                jsonFormat.append(c + "\n");
                jsonFormat.append(getSpaceOrTab(tabNum));
            } else if (c == ':') {
                jsonFormat.append(c + " ");
            } else if (c == '[') {
                tabNum++;
                char next = strJson.charAt(i + 1);
                if (next == ']') {
                    jsonFormat.append(c);
                } else {
                    jsonFormat.append(c + "\n");
                    jsonFormat.append(getSpaceOrTab(tabNum));
                }
            } else if (c == ']') {
                tabNum--;
                if (last == '[') {
                    jsonFormat.append(c);
                } else {
                    jsonFormat.append("\n" + getSpaceOrTab(tabNum) + c);
                }
            } else {
                jsonFormat.append(c);
            }
            last = c;
        }
        return jsonFormat.toString();
    }

    private static String getSpaceOrTab(int tabNum) {
        StringBuffer sbTab = new StringBuffer();
        for (int i = 0; i < tabNum; i++) {
            sbTab.append("  ");
        }
        return sbTab.toString();
    }


}
