package cn.ljcdada.deathassistant.email;

import java.io.Serializable;

public class EmailBean implements Serializable {
    private String host;//邮箱服务器地址
    private String port;//邮箱服务器端口
    private String formEmail;//发件人
    private String toEmail;//收件人
    private String password;//密码
    private String title;//主题
    private String app_name;//app名称
    private String user_name;//用户id
    private String messages;//邮件文本内容
    private String time;//崩溃时间
    private String json;//崩溃详细信息

    public EmailBean(String host, String port, String formEmail, String toEmail, String password, String title, String app_name, String user_name) {
        this.host = host;
        this.port = port;
        this.formEmail = formEmail;
        this.toEmail = toEmail;
        this.password = password;
        this.title = title;
        this.app_name = app_name;
        this.user_name = user_name;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getUser_name() {
        return user_name==null?"":user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public EmailBean() {
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getFormEmail() {
        return formEmail;
    }

    public void setFormEmail(String formEmail) {
        this.formEmail = formEmail;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
