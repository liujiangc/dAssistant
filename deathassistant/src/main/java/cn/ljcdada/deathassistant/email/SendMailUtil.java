package cn.ljcdada.deathassistant.email;

import java.io.File;

public class SendMailUtil {

    public static void send(final File file, EmailBean mEmail) {
        final MailInfo mailInfo = creatMail(mEmail);
        final MailSender sms = new MailSender();
        new Thread(new Runnable() {
            @Override
            public void run() {
                sms.sendFileMail(mailInfo, file);
            }
        }).start();
    }

    public static void send(EmailBean mEmail) {
        final MailInfo mailInfo = creatMail(mEmail);
        final MailSender sms = new MailSender();
        new Thread(new Runnable() {
            @Override
            public void run() {
                sms.sendHtmlMail(mailInfo);
            }
        }).start();
    }

    public static MailInfo creatMail(EmailBean mEmail) {
        final MailInfo mailInfo = new MailInfo();
        mailInfo.setMailServerHost(mEmail.getHost());
        mailInfo.setMailServerPort(mEmail.getPort());
        mailInfo.setValidate(true);
        mailInfo.setUserName(mEmail.getFormEmail()); // 你的邮箱地址
        mailInfo.setPassword(mEmail.getPassword());// 您的邮箱密码
        mailInfo.setFromAddress(mEmail.getFormEmail()); // 发送的邮箱
        mailInfo.setToAddress(mEmail.getToEmail()); // 发到哪个邮件去
        mailInfo.setSubject(mEmail.getTitle()); // 邮件主题
        mailInfo.setContent(mEmail.getMessages()); // 邮件文本
        return mailInfo;
    }

}
